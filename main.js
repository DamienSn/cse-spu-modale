/* 
<modal-info
            title="Modale"                              -> Titre de la modale
            content="Contenu de la modale"              -> Contenu de la modale
            btn-done="Ok"                               -> Bouton ne plus afficher
            btn-rappel="Me le rappeler"                 -> Bouton de rappel
            btn-more="Plus d'infos"                     -> Bouton plus d'infos
            btn-more-link="/"                           -> Lien vers les infos
            icon="fab fa-angellist"                     -> Icone de la modale (Voir https://fontawesome.com)
            local-storage-key="fete-musqiue-inscrit"    -> Clé à utiliser dans les cookies NE PAS UTILISER DEUX FOIS LA MEME
        >
</modal-info>
 */

class Modal extends HTMLElement {
    constructor() {
        super();
    }

    connectedCallback() {
        const title = this.getAttribute("title");
        const content = this.getAttribute("content");
        const icon = this.getAttribute("icon");
        const btnDone = this.getAttribute("btn-done");
        const btnRappel = this.getAttribute("btn-rappel");
        const btnInfos = this.getAttribute("btn-more");
        const btnInfosLink = this.getAttribute("btn-more-link");

        this.innerHTML = `
        <style>
        button a[href="${btnInfosLink}"] {
            color: #fff!important;
            text-decoration: none!important;
          }
          
          #plus-tard i {
            transform-origin: top center;
          }
          
          #plus-tard:hover i {
            animation: bell-ring 0.35s linear;
          }
          
          @keyframes bell-ring {
            25% {
              transform: rotate(35deg);
            }
            50%{
              transform: rotate(0deg);
            }
            75% {
              transform: rotate(-35deg);
            }
            100% {
              transform: rotate(0deg);
            }
          }

          #btn-modal {
              display: none;
          }
        </style>

        <!-- Button trigger modal -->
        <button type="button" id="btn-modal" class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#modal">
            Launch modal
        </button>

        <!-- Modal -->
        <div class="modal fade" id="modal" tabindex="-1" aria-labelledby="Modale" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel"><i class="${icon}"></i>&nbsp;${title}</h5>
            </div>
            <div class="modal-body">
                ${content}
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal" id="done">${btnDone}</button>
                <button type="button" class="btn btn-outline-primary" data-bs-dismiss="modal" id="plus-tard"><i class="fas fa-bell"></i>&nbsp;${btnRappel}</button>
                <button type="button" class="btn btn-primary"><a href="${btnInfosLink}">${btnInfos}</a></button>
            </div>
            </div>
        </div>
        </div>
        `;

        let script = document.createElement('script');
        script.textContent = `
        console.log('coucou');
        if(!localStorage.getItem('${this.getAttribute('local-storage-key')}')) {
            document.querySelector('#btn-modal').click()
        }

        function inscrire() {
            localStorage.setItem('${this.getAttribute('local-storage-key')}', true);
        }
          
        document.querySelector('#done').onclick = inscrire;
        `;
        this.appendChild(script);
    }
}

window.customElements.define("modal-info", Modal);
